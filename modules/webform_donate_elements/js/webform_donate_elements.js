/**
 * @file
 * Javascript behaviors for donation webform elements.
 */

(function ($, Drupal, drupalSettings) {
  "use strict";

  Drupal.behaviors.webformDonateElements = {
    attach: function (context, settings) {

      var frequencies = Object.keys(drupalSettings.webformDonateElements.amounts);
      var loadingSpinner = $("#webformLoading");
      var webform = $(".webform-submission-uk-caw-donate-widget-form");

      /**
       * Loading animation.
       */
      $(document).ready(function () {
        $(loadingSpinner).hide();
        $(webform).each(function () {
          this.style.setProperty('display', 'flex', 'important');
        })
      });

      /**
       * Setup amount benefit text.
       */
      $('#edit-amount-amount', context).once('webformDonateElementsAmount').each(function () {
        $.each(frequencies, function (index, frequency) {
          $("input[name='amount[amount][" + frequency + "][amounts][buttons]']").each(function () {
            var amount = $(this).val();
            $(this).click(function () {
              setBenefit(frequency, amount);
            });

            // Set benefit & hero images on page load.
            if ($(this).is(':checked') && $('#edit-amount-frequency-' + frequency.replace("_", "-")).is(':checked')) {
              setBenefit(frequency, amount);
              $('.webform-donate-elements-hero-image').hide();
              $('#edit-amount-' + frequency.replace("_", "-") + '-hero-image').show();
            }
          });
        });
      });

      // Set frequency amount toggling.
      $('input[name=\'amount[frequency]\']', context).once('webformDonateElementsFrequency').each(function () {
        var frequency = $(this).val();
        $(this).click(function () {
          toggleFrequency(frequency);
        });
      });

      /**
       * Set up toggling of payment method container when clicking payment method.
       */
      $('input[name=\'payment[payment_methods][one_off][selection]\']', context).once('webformDonateElementsPayment').each(function () {
        var el = $(this);
        el.click(function () {
          var method = el.val();
          $('.webform-donate-method').each(function () {
            var elmData = $(this).data();
            if (elmData['donationsMethod'] !== undefined && elmData['donationsMethod'] === method) {
              $(this).show();
            }
            else {
              $(this).hide();
            }
          });
        });
      });

      /**
       * Below fires jQuery validate (oly works for non-ajax).
       */
      $('.webform-button--next', context).once('webformDonateElementsButtonNext').each(function () {
        var el = $(this);
        el.click(function (event) {
          var form = $(this).closest('form');
          form.validate();
          if (!form.valid()) {
            event.preventDefault();
            event.stopPropagation();
            return false;
          }
        });
      });

      /**
       * Verify final form submission.
       */
      $('.webform-donate-payment-result',context).once('webformDonatePaymentResult').each(function () {
        var form = $(this).parents('form:first');
        var el = $(this);
        $(form).submit(function (event) {
          console.log('Clicked: ' + form.attr('name'));
          var result = JSON.parse(el.val());
          if (result.id === undefined) {
          };
          console.log(result);
          event.preventDefault();
        });
      });

      function setBenefit(frequency, amount) {
        if (frequency !== undefined && amount !== undefined) {
          var benefit = '';
          var benefitContainer = $('.webform_donate_elements_benefit');
          if (drupalSettings.webformDonateElements.amounts[frequency].amounts[amount] !== undefined) {
            benefit = drupalSettings.webformDonateElements.amounts[frequency].amounts[amount].benefit;
          }

          benefitContainer.text(benefit);
          if(benefit.length < 1) {
            benefitContainer.css('min-height', '0');
          } else {
            benefitContainer.css('min-height', '5.5rem');
          }

        }
      }

      function toggleFrequency(frequency) {
        if (frequency !== undefined) {
          var amount;
          for (var item in frequencies) {
            amount = $('input[name=\'amount[amount][' + frequency + '][amounts][buttons]\']:checked').val();
            setBenefit(frequency, amount);
            if (frequencies[item] === frequency) {
              $('.webform_donate_amount-' + frequencies[item]).show();
            }
            else {
              $('.webform_donate_amount-' + frequencies[item]).hide();
            }
          }

          $('.webform-donate-elements-hero-image').hide();
          $('#edit-amount-' + frequency.replace("_", "-") + '-hero-image').show();
          $('#edit-amount-hero-image-' + frequency.replace("_", "-")).show();
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
