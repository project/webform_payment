<?php

namespace Drupal\webform_donate_elements\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Number;

/**
 * Provides a textfield element with input mask and min.
 *
 * @FormElement("webform_donate_amount_textfield")
 */
class WebformDonateAmountTextfield extends Number {

  /**
   * {@inheritdoc}
   */
  protected static $type = 'number';

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $class = get_class($this);
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderNumber($element) {
    $element = parent::preRenderNumber($element);

    // Let min through.
    Element::setAttributes($element, ['min']);

    return $element;
  }

}
