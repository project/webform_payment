<?php

namespace Drupal\webform_donate_elements\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformButtonsOther;

/**
 * Provides a webform element for buttons with an other option.
 *
 * @FormElement("webform_donate_amount_buttons_other")
 */
class WebformDonateAmountButtonsOther extends WebformButtonsOther {

  /**
   * {@inheritdoc}
   */
  public static function validateWebformOther(&$element, FormStateInterface $form_state, &$complete_form) {
    $frequency_parents = [$element['#parents'][0], 'frequency'];
    $frequency = NestedArray::getValue($form_state->getValues(), $frequency_parents);

    // Only provide validation if it is for the currently selected frequency.
    if ($element['#parents'][2] == $frequency) {
      return parent::validateWebformOther($element, $form_state, $complete_form);
    }
  }

}
