<?php

namespace Drupal\webform_donate_payment\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;

/**
 * Payment method integration.
 *
 * @WebformHandler(
 *   id = "donate_payment_methods",
 *   label = @Translation("Donate payment methods"),
 *   category = @Translation("Webform donate"),
 *   description = @Translation("Enabled payment methods from payment API
 *   providing bridge functionality from webform to use a payment method."),
 *   cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class DonatePaymentMethodsHandler extends WebformHandlerBase {

  /**
   * List of payment frequency types.
   *
   * @var array
   */
  protected $paymentFrequencies;

  /**
   * Payment Method Manager
   *
   * @var $payment_method_manager
   *   \Drupal\payment\Plugin\Payment\Method\PaymentMethodManager
   */
  protected $paymentMethodManager;


  /**
   * DonatePaymentMethodsHandler constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param LoggerChannelFactoryInterface $logger_factory
   * @param ConfigFactoryInterface $config_factory
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param WebformSubmissionConditionsValidatorInterface $conditions_validator
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator) {

    // Payment frequencies.
    // TODO: Build payment frequencies class.
    $this->paymentFrequencies = [
      'one_off' => [
        'label' => $this->t('One off'),
        'default_option_label' => $this->t('One off'),
        'prefix' => 'donation',
        'columns' => [
          'transaction_id',
          'payment_method',
        ],
      ],
      'recurring' => [
        'label' => $this->t('Recurring'),
        'default_option_label' => $this->t('Recurring'),
        'prefix' => 'dd',
        'columns' => [
          'first_payment_date',
          'frequency',
          'sort_code',
          'account_number',
          'account_name',
          'instruction_reference',
        ],
      ],
    ];

    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->paymentMethodManager = \Drupal::service('plugin.manager.payment.method');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'payment_methods' => [],
      ];
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment_methods = [];
    $any_status = FALSE;
    foreach ($this->getPaymentMethods($any_status) as $plugin_id => $definition) {
      $row = [
        '#type' => 'fieldset',
        '#title' => $definition['label'],
        'status' => [
          '#markup' => t('Payment API status: ') . ($definition['active'] ? $this->t('Enabled') : $this->t('Disabled')),
        ],
      ];

      if (!$definition['active']) {
        $row['#attributes']['class'] = ['payment-method-disabled'];
      }
      else {
        $row['frequencies_text'] = [
          '#markup' => '<br /><br />' . t('Payment frequencies: '),
        ];
        foreach ($this->paymentFrequencies as $frequency_id => $payment_frequency) {
          $row['frequencies'][$frequency_id]['enabled'] = [
            '#type' => 'checkbox',
            '#title' => $payment_frequency['label'],
            '#default_value' => $this->getPaymentFrequencyStatus($definition['id'], $frequency_id),
          ];
        }
      }
      $payment_methods[$definition['id']] = $row;
    }

    $form['payment_methods'] = $payment_methods;
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['payment_methods'] = $values['payment_methods'];

  }

  /**
   * Helper function to calculate payment method status.
   *
   * @param $payment_method_id string
   *
   * @return bool
   */
  public function getPaymentMethodStatus($payment_method_id) {
    $payment_methods = $this->getPaymentMethods();
    return (isset($payment_methods[$payment_method_id])? TRUE : FALSE);
  }

  /**
   * Helper function to calculate methods frequency status.
   *
   * @param $payment_method_id string
   * @param $frequency_id string
   *
   * @return bool
   */
  public function getPaymentFrequencyStatus($payment_method_id, $frequency_id) {
    $frequency = (!empty($this->configuration['payment_methods'][$payment_method_id]['frequencies'][$frequency_id]) ? $this->configuration['payment_methods'][$payment_method_id]['frequencies'][$frequency_id] : []);
    $enabled = TRUE;
    $not_enabled = FALSE;
    return (!empty($frequency['enabled']) && $frequency['enabled'] === 1) ? $enabled : $not_enabled;
  }

  /**
   * Get the available payment types.
   *
   * @param mixed $key
   *   Key of frequency to return.
   *
   * @return array|false
   *   The payment types, keyed by the payment type id.
   */
  public function getPaymentFrequencies($key = FALSE) {
    if (!empty($key)) {
      return !empty($this->paymentFrequencies[$key]) ? $this->paymentFrequencies[$key] : FALSE;
    }
    else {
      return $this->paymentFrequencies;
    }
  }

  /**
   * Get payment methods manager
   *
   * @return \Drupal\payment\Plugin\Payment\Method\PaymentMethodManager
   */
  public function getPaymentMethodsManager() {
    return $this->paymentMethodManager;
  }

  /**
   * Get Payment methods, enabled or any.
   *
   * @var bool $enabled
   *
   * @return array
   *  Array of enabled payment methods.
   */
  public function getPaymentMethods($enabled = TRUE) {
    $payment_methods = [];
    foreach ($this->paymentMethodManager->getDefinitions() as $id => $method) {
      if ($enabled) {
        if (!empty($method['active']) && $method['active']) {
          $payment_methods[$id] = $method;
        }
      }
      else {
        $payment_methods[$id] = $method;
      }
    }
    return $payment_methods;
  }
}

