<?php

namespace Drupal\webform_donate_payment\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 * Provides a 'webform_donate_payment' element.
 *
 * @WebformElement(
 *   id = "webform_donate_payment",
 *   label = @Translation("Webform donate payment"),
 *   description = @Translation("Provides a webform donate payment element."),
 *   category = @Translation("Webform donation payment element"), multiline =
 *   FALSE, composite = TRUE, states_wrapper = TRUE,
 * )
 *
 */
class WebformDonatePayment extends WebformCompositeBase {

  //todo: Build out.

}


