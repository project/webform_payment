<?php

namespace Drupal\webform_donate_payment\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'webform_donate_payment' element.
 *
 * Webform composites contain a group of sub-elements.
 *
 **
 * @FormElement("webform_donate_payment")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 */
class WebformDonatePayment extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [];
  }

  // todo: build out.
}
